# Fisa Autoturism

That is romanian for _Car File_.

So Basically this an essay on managing a company's car park. 

Not if you are a transport company though.

## What it does

This helps you manage the cars, the drivers and the routes they take. Although you can manage the fuel suppliers.

## How

It uses only backbone (and it's dependencies: underscore and jquery) and bootstrap.

