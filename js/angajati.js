var Angajat = Backbone.Model.extend({
  url:"/angajati",
  defaults:{
    name: "",
    position : ""
  },
  className: "row",
  validate: function(attrs){
    var errors = [];
    if(!attrs.name){
      errors.push({name:"addName", message:"Trebuie sa introduceti numele angajatului!"});
    }
    if(!attrs.position){
      errors.push({name:"addPosition",message:"Trebuie sa introduceti functia angajatului!"});
    }
    return errors.length > 0 ? errors: false;
  }
});

var Angajati = Backbone.Collection.extend({
    model: Angajat
});

var angajat1 = new Angajat({
  name: "Mircea Nistor",
  position: "Head of security"
});

var AngajatView = Backbone.View.extend({
  tagName:"tr",
  model: new Angajat({name:"",position:""}),
  template:_.template($("#tmplAngajat").html()),
  initialize:function(){
    this.render();
  },
  events: {
    "click .editAngajat": 'editAngajat',
    "click .cancelAngajat": 'cancelAngajat',
    "click .delAngajat": 'delAngajat',
    "click .saveAngajat": 'saveAngajat'
  },
  editAngajat: function(e){
    $(".editAngajat").hide();
    $(".delAngajat").hide();
    this.$(".saveAngajat").show();
    this.$(".cancelAngajat").show();

    name=this.$(".name").html();
    position=this.$(".position").html();

    this.$(".name").html("<input type='TEXT' class='form-control name-update' value='"+name+"'/>");
    this.$(".position").html("<input type='TEXT' class='form-control position-update' value='"+position+"'/>");
  },
  cancelAngajat: function(e){
    $(".editAngajat").show();
    $(".delAngajat").show();
    $(".saveAngajat").hide();
    $(".cancelAngajat").hide();

    this.render();
  },
  delAngajat : function(e){
    if(!confirm("Sigur doriti stergerea angajatului selectat?")){
      this.cancelAngajat();
      return;
    }
    this.model.destroy();
  },
  saveAngajat: function(){
    this.model.set("name",this.$(".name-update").val());
    this.model.set("position",this.$(".position-update").val());
    this.render();
    this.cancelAngajat();
  },
  render: function(){
    var data = this.model ? this.model.toJSON() : {};
    this.$el.html(this.template(data));
    return this;
  }
});

var angajati = new Angajati([angajat1]);

var AngajatiView = Backbone.View.extend({
  el: $("#container"),
  model: angajati,
  template: _.template($("#tmplAngajati").html()),
  initialize: function(options){
    this.model.on("remove",this.render,this);
    self = this;
    this.model.on("invalid",function(error){
      var err = "";
      self.clearErrorSigns();
      _.each(error.validationError, function(error){
        err+=error.message+"\n";
        $("#div"+error.name).addClass("has-error");
      });
      alert(err);
    });
  },
  clearErrorSigns: function(){
    $("div .form-group").removeClass("has-error");
  },
  events:{
    "click #addAngajat":'add'
  },
  add: function(e){
    e.preventDefault();
    var a = new Angajat({
      name:$("#addName").val(),
      position:$("#addPosition").val()
    });

    if(this.model.create(a).validationError==null){
      this.render();
    }
    //console.log(this.model.add([a],{validate:true})[0]);
  },
  render: function(){
    $("#MainMenu").find("li").removeClass("active");
    $('a[href="#Angajati"]').parent().addClass("active");

    $("#header").html("");

    var data = this.model ? this.model.toJSON() : {};
    this.$el.html(this.template(data));
    _.each(this.model.toArray(),function(a){
      if(a.validationError==null){
        $("#tblAngajati tbody").append(new AngajatView({model:a}).render().el);
      }
    });
    return this;
  }

});
