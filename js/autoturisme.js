var Autoturism = Backbone.Model.extend({
  url: "/autoturisme",
  defaults:{
    nr:"",
    type:"",
    city_consumption:0,
    out_of_city_consumption:0,
    mixt_consumption:0,
    fuel_type:""
  },
  className: "row",
  validate: function(attrs){
    var errors = [];
    if(!attrs.nr){
      errors.push({name:"addNr", message:"Trebuie sa introduceti numarul masinii!"});
    }
    if(!attrs.type){
      errors.push({name:"addType",message:"Trebuie sa introduceti marca masinii!"});
    }
    if(isNaN(attrs.city_consumption) || attrs.city_consumption==""){
      errors.push({name:"addCityConsumption",message:"Trebuie sa introduceti un consum in oras!"});
    }
    if(isNaN(attrs.out_of_city_consumption) || attrs.out_of_city_consumption==""){
      errors.push({name:"addOutOfCityConsumption",message:"Trebuie sa introduceti un consum extraurban!"});
    }
    if(isNaN(attrs.mixt_consumption) || attrs.mixt_consumption==""){
      errors.push({name:"addMixtConsumption",message:"Trebuie sa introduceti un consum mixt!"});
    }
    if(attrs.fuel_type!='Benzina' && attrs.fuel_type!='Diesel'){
      errors.push({name:"addFuelType",message:"Trebuie sa introduceti un combustibil!"});
    }
    return errors.length > 0 ? errors: false;
  }

});

var Autoturisme = Backbone.Collection.extend({
  model: Autoturism
});

var autoturisme = new Autoturisme([{nr:"AR-48-SLC",type:"skoda",city_consumption:"2",
  out_of_city_consumption:"2",mixt_consumption:"2",fuel_type:"Benzina"},
  {nr:"AR-11-NNG",type:"VW",city_consumption:"2",
    out_of_city_consumption:"2",mixt_consumption:"2",fuel_type:"Benzina"}]);

var AutoturismView = Backbone.View.extend({
  tagName:"tr",
  template: _.template($("#tmplAutoturism").html()),
  model: new Autoturism({nr:"AR-48-SLC",type:"",city_consumption:"",
    out_of_city_consumption:"",mixt_consumption:"",fuel_type:"Benzina"}),
  events: {
    "click .editAutoturism": 'editAuto',
    "click .cancelAutoturism": 'cancelAuto',
    "click .delAutoturism": 'delAuto',
    "click .saveAutoturism": 'saveAuto'
  },
  editAuto: function(e){
    $(".editAutoturism").hide();
    $(".delAutoturism").hide();
    this.$(".saveAutoturism").show();
    this.$(".cancelAutoturism").show();

    nr=this.$(".nr").html();
    type=this.$(".type").html();
    city_consumption=this.$(".city_consumption").html();
    out_of_city_consumption=this.$(".out_of_city_consumption").html();
    mixt_consumption=this.$(".mixt_consumption").html();
    fuel_type=this.$(".fuel_type").html();

    this.$(".nr").html("<input type='TEXT' class='form-control nr-update' value='"+nr+"'/>");
    this.$(".type").html("<input type='TEXT' class='form-control type-update' value='"+type+"'/>");
    this.$(".city_consumption").html("<input type='TEXT' class='form-control city_consumption-update' value='"+city_consumption+"'/>");
    this.$(".out_of_city_consumption").html("<input type='TEXT' class='form-control out_of_city_consumption-update' value='"+out_of_city_consumption+"'/>");
    this.$(".mixt_consumption").html("<input type='TEXT' class='form-control mixt_consumption-update' value='"+mixt_consumption+"'/>");
    this.$(".fuel_type").html("<select class='form-control fuel_type-update'><option value='Benzina' "+(fuel_type=="Benzina"?"selected":"")+">Benzina</option><option value='Diesel' "+(fuel_type=="Diesel"?"selected":"")+">Diesel</option></select>");
  },
  cancelAuto: function(e){
    $(".editAutoturism").show();
    $(".delAutoturism").show();
    $(".saveAutoturism").hide();
    $(".cancelAutoturism").hide();

    this.render();
  },
  delAuto : function(e){
    if(!confirm("Sigur doriti stergerea autoturismului selectat?")){
      this.cancelAuto();
      return;
    }
    this.model.destroy();
  },
  saveAuto: function(){
    this.model.set("nr",this.$(".nr-update").val());
    this.model.set("type",this.$(".type-update").val());
    this.model.set("city_consumption",this.$(".city_consumption-update").val());
    this.model.set("out_of_city_consumption",this.$(".out_of_city_consumption-update").val());
    this.model.set("mixt_consumption",this.$(".mixt_consumption-update").val());
    this.model.set("fuel_type",this.$(".fuel_type-update").val());
    this.render();
    this.cancelAuto();
  },
  initialize: function(){
    this.listenTo(this.model,"remove",this.remove);
    this.render();
  },
  render: function(){
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  }
});

var AutoturismeView = Backbone.View.extend({
  el: $("#container"),
  template: _.template($("#tmplAutoturisme").html()),
  model: autoturisme,
  initialize: function(){
    this.render();
    self = this;
    this.model.on("invalid",function(error){
      var err = "";
      self.clearErrorSigns();
      _.each(error.validationError, function(error){
        err+=error.message+"\n";
        $("#div"+error.name).addClass("has-error");
      });
      alert(err);
    });

    this.model.on("remove",this.render,this);

  },
  clearErrorSigns: function(){
    $("div .form-group").removeClass("has-error");
  },
  events:{
    "click #addAutoturism":'add'
  },
  add: function(e){
    e.preventDefault();
    var a = new Autoturism({
      nr:$("#addNr").val(),
      type:$("#addType").val(),
      city_consumption:$("#addCityConsumption").val(),
      out_of_city_consumption:$("#addOutOfCityConsumption").val(),
      mixt_consumption:$("#addMixtConsumption").val(),
      fuel_type:$("#addFuelType").val()
    });
    if(this.model.create(a).validationError==null){
      this.render();
    }
    //console.log(this.model.add([a],{validate:true})[0]);
  },
  render: function(){
    $("#MainMenu").find("li").removeClass("active");
    $('a[href="#Autoturisme"]').parent().addClass("active");

    $("#header").html("");

    this.$el.html(this.template(this.model));
    _.each(this.model.toArray(),function(a){
      if(a.validationError==null){
        $("#tblAutoturisme tbody").append(new AutoturismView({model:a}).render().el);
      }
    });
    return this;
  }

});
