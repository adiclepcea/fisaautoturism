var cursaCurenta;
var cursaCurentaView;
var Cursa = Backbone.Model.extend({
  url:"/curse",
  defaults:{
    number : "",
    date: new Date(),
    confirmation: "",
    startKm: 0,
    finishKm: 100,
    startFuel: 50,
    finishFuel:22,
    auto_nr:"AR-48-SLC",
    auto_type:"skoda",
    auto_city_consumption:"2",
    auto_out_of_city_consumption:"2",
    auto_mixt_consumption:"2",
    auto_fuel_type:"Benzina",
    nou: true
  },
  validate: function(attrs,option){
      var errors = Array();

      var now = new Date();
      if(attrs.date==null || now<new Date(attrs.date)){
        errors.push("Data nu este corecta!"+formatDate(new Date(attrs.data),".")+" <=> " + formatDate(now,"."));
      }
      if(isNaN(attrs.number) || attrs.number<=0){
        errors.push("Numar incorect");
      }
      if(isNaN(attrs.startKm) || attrs.startKm<=0){
        errors.push("Km plecare incorecti");
      }
      if(isNaN(attrs.finishKm) || attrs.finishKm<=0){
        errors.push("Km sosire incorecti");
      }
      if(isNaN(attrs.startFuel) || attrs.startFuel<=0){
        errors.push("Combustibil la plecare incorecti");
      }
      if(isNaN(attrs.finishFuel) || attrs.finishFuel<=0){
        errors.push("Combustibil la sosire incorecti");
      }
      if(errors.length>0){
        return errors;
      }
    },
    save: function(key, val, options){
      var r = Backbone.Model.prototype.save.call(this, key, val, options);
      if(r){
        if(this.get("nou")){
          curse.push(this);
        }else{
          cursaViewCurenta.oldModel.set(this.attributes);
        }
      }
      return r;
    }
});

var Curse = Backbone.Collection.extend({
  model: Cursa
});

var curse = new Curse([{
  number : "1",
  date: "2016-07-20",confirmation: "",startKm: 1000,finishKm: 1100,
  startFuel: 54,finishFuel:21,
  auto_nr:"AR-48-SLC",auto_type:"skoda",auto_city_consumption:"2",
    auto_out_of_city_consumption:"2",auto_mixt_consumption:"2",auto_fuel_type:"Benzina",
    ruteRows : [{idRuta:0, ruta:"Arad-Curtici-Arad",
      dataPlecare:new Date(),dataSosire: new Date(), kmUrban: 5,
      kmExtraUrban:12, scop:"Plimbare",confirmare:"Sef departament",
      idPersonal:1,sofer:"Mircea"},
      {idRuta:0, ruta:"Curtici-Ineu-Curtici",
        dataPlecare:new Date("2016-08-19"),dataSosire: new Date("2016-08-20"), kmUrban: 10,
        kmExtraUrban:52, scop:"Aprovizionare",confirmare:"Sef departament",
        idPersonal:1,sofer:"Mircea"}],
    alimRows : [{
      furnizor: "OMV", data: new Date(),
      nrDocument: 10234,  cantitate: 20, pretUnitar: 5.2
    }]
},{
  number : "2",
  date: "2016-08-10",confirmation: "",startKm: 0,finishKm: 0,
  startFuel: 0,finishFuel:0,
  auto_nr:"AR-48-SLC",auto_type:"skoda",auto_city_consumption:"2",
    auto_out_of_city_consumption:"2",auto_mixt_consumption:"2",auto_fuel_type:"Benzina",
  ruteRows: [],
  alimRows: []
},{
  number : "3",
  date: "2016-08-10",confirmation: "",startKm: 0,finishKm: 0,
  startFuel: 0,finishFuel:0,
  auto_nr:"AR-11-NNG",auto_type:"VW",auto_city_consumption:"2",
    auto_out_of_city_consumption:"2",auto_mixt_consumption:"2",auto_fuel_type:"Benzina",
  ruteRows: [],
  alimRows: []
}]);

var AlimRow = Backbone.Model.extend({
  url:"/fake",
  defaults : {
    furnizor: "",
    data: new Date(),
    nrDocument: 1,
    cantitate: 0,
    pretUnitar: 0
  },
  validate: function(attrs,option){
    var errors = Array();
    if(attrs.furnizor == ""){
      errors.push("Furnizorul trebuie specificat!");
    }
    var now = new Date();
    if(attrs.data==null || now<new Date(attrs.data)){
      errors.push("Data nu este corecta!"+formatDate(new Date(attrs.data),".")+" <=> " + formatDate(now,"."));
    }
    if(isNaN(attrs.cantitate) || attrs.cantitate<=0){
      errors.push("cantitate incorecta")
    }
    if(isNaN(attrs.pretUnitar) || attrs.pretUnitar<=0){
      errors.push("Pretul per Litru este incorect")
    }

    if(errors.length>0){
      return errors;
    }
  }
});

var RutaRow = Backbone.Model.extend({
  url: "/fake",
  defaults : {
    idRuta: 0,
    ruta: "",
    dataPlecare: new Date(),
    dataSosire: new Date(),
    kmUrban: 0,
    kmExtraUrban: 0,
    scop:"",
    confirmare:"",
    idPersonal: 0,
    sofer: ""
  },
  validate: function(attrs,option){
    var errors = Array();

    if(attrs.ruta == ""){
      errors.push("Ruta trebuie specificata!");
    }

    if(attrs.dataPlecare==null || attrs.dataSosire==null){
      errors.push("Datele de plecare si sosire trebuie specificate!");
    }
    if(isNaN(attrs.kmUrban) || attrs.kmUrban<=0){
      errors.push("Campul Km urban este incorect!");
    }
    if(isNaN(attrs.kmExtraUrban) || attrs.kmExtraUrban<=0){
      errors.push("Campul Km Extraurban este incorect!");
    }

    if(errors.length>0){
      return errors;
    }
  }
});

var CursaAlimRowView = Backbone.View.extend({
  tagName : "tr",
  template: _.template($("#tmplCurseAlimentariRow").html()),
  model : new AlimRow(),
  events:{
    "click .editAlimRow": 'editAlim',
    "click .deleteAlimRow": 'deleteAlim',
    "click .cancelAlimRow": 'cancelAlim',
    "click .saveAlimRow": 'saveAlim'
  },
  deleteAlim: function(){
    var alimRows = cursaCurenta.get("alimRows");
    alimRows.splice(alimRows.indexOf(this.model),1);
    cursaCurenta.set("alimRows",alimRows);
    showAlimRows(alimRows);
  },
  editAlim: function(){
    this.$(".editAlimRow").hide();
    this.$(".deleteAlimRow").hide();
    this.$(".saveAlimRow").show();
    this.$(".cancelAlimRow").show();
    this.$(".alimRowFurnizor").html("<select class='editAlimRowFurnizor form-control'></select>");
    this.$(".alimRowData").html("<input type='text' class='editAlimRowData form-control' value='"+this.model["data"]+"'>");
    this.$(".alimRowNrDoc").html("<input type='text' class='editAlimRowNrDocument form-control' value='"+this.model["nrDocument"]+"'>");
    this.$(".alimRowCant").html("<input type='text' class='editAlimRowCantitate form-control' value='"+this.model["cantitate"]+"'>");
    this.$(".alimRowPretL").html("<input type='text' class='editAlimRowPretUnitar form-control' value='"+this.model["pretUnitar"]+"'>");

    self = this;
    _.each(furnizori.toArray(),function(a){
      self.$(".editAlimRowFurnizor").append("<option value=\""+a.get("name")+"\">"+a.get("name")+"</option>");
    });

    this.$('.editAlimRowFurnizor option[value="'+this.model["furnizor"]+'"]').attr('selected','selected');

    flatpickr(".editAlimRowData");

  },
  cancelAlim: function(){
    this.render();
  },
  saveAlim: function(){
    this.model["furnizor"] = this.$(".editAlimRowFurnizor").val();
    this.model["data"] = new Date(this.$(".editAlimRowData").val());
    this.model["nrDoc"] = new Date(this.$(".editAlimRowNrDocument").val());
    this.model["cantitate"] = Number(this.$(".editAlimRowCantitate").val());
    this.model["pretUnitar"] = Number(this.$(".editAlimRowPretUnitar").val());
    this.render();
  },
  render: function(){
    this.$el.html(this.template(this.model));
    return this;
  }
});

var CursaRuteRowView = Backbone.View.extend({
  tagName : "tr",
  template: _.template($("#tmplCurseRuteRow").html()),
  model : new RutaRow(),
  events:{
    "click .editRuteRow": 'editRuta',
    "click .deleteRuteRow": 'deleteRuta',
    "click .cancelRuteRow": 'cancelRuta',
    "click .saveRuteRow": 'saveRuta'
  },
  deleteRuta: function(){
    var ruteRows = cursaCurenta.get("ruteRows");
    ruteRows.splice(ruteRows.indexOf(this.model),1);
    cursaCurenta.set("ruteRows",ruteRows);
    showRuteRows(ruteRows);
  },
  editRuta: function(){
    this.$(".editRuteRow").hide();
    this.$(".deleteRuteRow").hide();
    this.$(".saveRuteRow").show();
    this.$(".cancelRuteRow").show();
    this.$(".ruteRowRuta").html("<input type='text' class='editRuteRowRuta form-control' value='"+this.model["ruta"]+"'>");
    this.$(".ruteRowDataPlecare").html("<input type='text' class='editRuteRowDataPlecare form-control' value='"+formatDateDefault(this.model["dataPlecare"],".")+"'>");
    this.$(".ruteRowDataSosire").html("<input type='text' class='editRuteRowDataSosire form-control' value='"+formatDateDefault(this.model["dataSosire"],".")+"'>");
    this.$(".ruteRowKmUrban").html("<input type='text' class='editRuteRowKmUrban form-control' value='"+this.model["kmUrban"]+"'>");
    this.$(".ruteRowKmExtraurban").html("<input type='text' class='editRuteRowKmExtraurban form-control' value='"+this.model["kmExtraUrban"]+"'>");
    this.$(".ruteRowScop").html("<input type='text' class='editRuteRowScop form-control' value='"+this.model["scop"]+"'>");
    this.$(".ruteRowConfirmare").html("<input type='text' class='editRuteRowConfirmare form-control' value='"+this.model["confirmare"]+"'>");
    this.$(".ruteRowSofer").html("<select class='editRuteRowSofer form-control'></select>");
    self = this;
    _.each(angajati.toArray(),function(a){
      self.$(".editRuteRowSofer").append("<option value=\""+a.get("name")+"\">"+a.get("name")+"</option>");
    });

    this.$('.editRuteRowSofer option[value="'+this.model["sofer"]+'"]').attr('selected','selected');

    flatpickr(".editRuteRowDataPlecare");
    flatpickr(".editRuteRowDataSosire");

  },
  cancelRuta: function(){
    this.render();
  },
  saveRuta: function(){
    this.model["ruta"] = this.$(".editRuteRowRuta").val();
    this.model["dataPlecare"] = new Date(this.$(".editRuteRowDataPlecare").val());
    this.model["dataSosire"] = new Date(this.$(".editRuteRowDataSosire").val());
    this.model["kmUrban"] = Number(this.$(".editRuteRowKmUrban").val());
    this.model["kmExtraUrban"] = Number(this.$(".editRuteRowKmExtraurban").val());
    this.model["scop"] = this.$(".editRuteRowScop").val();
    this.model["confirmare"] = this.$(".editRuteRowConfirmare").val();
    this.model["sofer"] = this.$(".editRuteRowSofer").val();
    this.render();
  },
  render: function(){
    this.$el.html(this.template(this.model));
    return this;
  }
});

var CursaAlimentariView = Backbone.View.extend({
  tagName:"tr",
  render: function(){
    $("#alimFurnizor").children().remove().end();
    $("#cursaSofer").children().remove().end();

    //populate supplier select
    _.each(furnizori.toArray(),function(f){
      $("#alimFurnizor").append("<option value=\""+f.get("name")+"\">"+f.get("name")+"</option>");
    });
    //populate driver select
    _.each(angajati.toArray(),function(a){
      $("#cursaSofer").append("<option value=\""+a.get("name")+"\">"+a.get("name")+"</option>");
    });
  }
});

var filteredAuto = new Curse();

var CurseHeaderView = Backbone.View.extend({
  el:$("#header"),
  template: _.template($("#tmplCurseHeader").html()),
  events:{
    "click #btnAdaugaCursa" : 'addCursa',
    "click #btnCautaCursa" : 'cautaCursa'
  },
  cautaCursa: function(){
    filteredAuto.reset(null);
    var di = new Date("1970-01-01");
    var ds = new Date();
    if($("#dataInceput").val()!=""){
      di = new Date($("#dataInceput").val());
    }
    if($("#dataSfarsit").val()!=""){
      ds = new Date($("#dataSfarsit").val());
    }
    //filter the routes so that only the selected ones are shown
    _.each(curse.toArray(),function(c){
      if($("#curseAutomobile").val()===c.get("auto_nr")){
        var cursaDate = new Date(c.get("date"));
        if(di<=cursaDate && ds>=cursaDate){
          filteredAuto.add(c);
        }
      }
    });
    filteredAuto.trigger("change");
  },
  addCursa: function(){
    //clear the fuelling and routes in the modal dialog so that a new travel can be instroduced
    $("#tblCursaAlimentari tbody").replaceWith("<tbody></tbody>");
    $("#tblCursaRute tbody").replaceWith("<tbody></tbody>");
    $("#cursaAutomobile").prop('disabled', false);
    $("#kmPlecare").val("0");
    $("#kmSosire").val("0");
    $("#combPlecare").val("0");
    $("#combSosire").val("0");
    $("#nrAutomobilCursa").val("0");
    $("#dataCursa").val(formatDateDefault(new Date()));
    $("#confirmareCursa").val("");

    cursaCurenta = new Cursa({
      number : "0",
      date: new Date(),confirmation: "",startKm: 0,finishKm: 0,
      startFuel: 0,finishFuel:0,
      auto_nr:"",auto_type:" ",auto_city_consumption:0,
        auto_out_of_city_consumption:0,auto_mixt_consumption:0,auto_fuel_type:"",
        ruteRows : [],
        alimRows : [],
        nou : true
    });
  },
  render: function(){

    this.$el.html(this.template({}));
    flatpickr("#alimData");
    flatpickr("#dataInceput");
    flatpickr("#dataSfarsit");
    flatpickr("#cursaRutaDataPlecare");
    flatpickr("#cursaRutaDataSosire");
    $("#cursaAutomobile").children().remove().end();

    _.each(autoturisme.toArray(), function(auto){
      $("#curseAutomobile").append("<option value=\""+auto.get("nr")+"\">"+auto.get("nr")+" - "+auto.get("type")+"</option>");
      $("#cursaAutomobile").append("<option value=\""+auto.get("nr")+"\">"+auto.get("nr")+" - "+auto.get("type")+"</option>");
    });
    return this;
  }
});

function showAlimRows(alimRows){
  $("#tblCursaAlimentari tbody").children().remove().end();
  _.each(alimRows,function(alim){
    $("#tblCursaAlimentari tbody").append(new CursaAlimRowView({model:alim}).render().el);
  });
}

function showRuteRows(ruteRows){
  $("#tblCursaRute tbody").children().remove().end();
  _.each(ruteRows,function(ruta){
    $("#tblCursaRute tbody").append(new CursaRuteRowView({model:ruta}).render().el);
  });
}

var CursaView = Backbone.View.extend({
    tagName: "tr",
    template: _.template($("#tmplCursa").html()),
    model: new Cursa(),
    initialize: function(){
      this.model.on("change",this.render,this);
    },
    events: {
      "click .editCursa": 'editCursa',
      "click .stergeCursa": 'stergeCursa'
    },
    editCursa: function(){
      //populate the fuelling from the selected model
      showAlimRows(this.model.get("alimRows"));
      //populate the routes from the selected model
      showRuteRows(this.model.get("ruteRows"));
      this.model.set("nou",false);
      //select the right auto
      $("#cursaAutomobile").val(this.model.get("auto_nr"));
      $("#cursaAutomobile").prop('disabled', 'disabled');
      $("#kmPlecare").val(this.model.get("startKm"));
      $("#kmSosire").val(this.model.get("finishKm"));
      $("#combPlecare").val(this.model.get("startFuel"));
      $("#combSosire").val(this.model.get("finishFuel"));
      $("#nrAutomobilCursa").val(this.model.get("number"));
      $("#dataCursa").val(this.model.get("date"));
      $("#confirmareCursa").val(this.model.get("confirmare"));
      this.oldModel = this.model;
      this.model = this.oldModel.clone();
      cursaCurenta = this.model;
      cursaCurenta.on("invalid", function(model,error){
        var strErr = "";
        _.each(error, function(err){
          strErr+=err+";\r\n";
        });
        alert(strErr);
      });
      cursaViewCurenta = this;
    },
    stergeCursa: function(){
      if(confirm("Sigur doriti stergerea cursei?")){
        this.model.destroy();
        filteredAuto.trigger("change");
      }
    },
    render: function(){

      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }
});

var CurseView = Backbone.View.extend({
  el:$("#container"),
  initialize: function(){
    this.model.on("change",this.render,this);
  },
  model: filteredAuto,
  template: _.template($("#tmplCurse").html()),
  render: function(){
    $("#MainMenu").find('li').removeClass("active");
    $('a[href="#Curse"]').parent().addClass("active");

    data = this.model?this.model:{};
    this.$el.html(this.template(data));

    _.each(this.model.toArray(),function(cursa){
      $("#tblCurse tbody").append(new CursaView({model:cursa}).render().el);
    });
    return this;
  }
});

function addNewAlim(){
    var alim = new AlimRow({
      furnizor: $("#alimFurnizor").val(),
      data: new Date($("#alimData").val()),
      nrDocument: $("#alimNrDoc").val(),
      cantitate: $("#alimCant").val(),
      pretUnitar: $("#alimPretPerL").val()
    });

  alim.on("invalid",function(model,error){
    var strErr = "";
    _.each(error, function(err){
      strErr+=err+";\r\n";
    });
    alert(strErr);
    return;
  });

  alim.save();
  if(alim.validationError!=null){
      return;
  }
  alimRows = cursaCurenta.get("alimRows");
  alimRows.push(alim.toJSON());
  cursaCurenta.set("alimRows",alimRows);
  showAlimRows(alimRows);
  console.log(alim);
}

function addNewRuta(){
  var ruta = new RutaRow({
    idRuta: 0,
    ruta: $("#cursaRuta").val(),
    dataPlecare: new Date($("#cursaRutaDataPlecare").val()),
    dataSosire: new Date($("#cursaRutaDataSosire").val()),
    kmUrban: $("#cursaKmUrban").val(),
    kmExtraUrban: $("#cursaKmExtraurban").val(),
    scop:$("#cursaScop").val(),
    confirmare:$("#cursaConfirmare").val(),
    idPersonal: 0,
    sofer: $("#cursaSofer").val()
    });

  ruta.on("invalid",function(model,error){
    var strErr = "";
    _.each(error, function(err){
      strErr+=err+";\r\n";
    });
    alert(strErr);
    return;
  });

  ruta.save();
  if(ruta.validationError!=null){
      return;
  }
  ruteRows = cursaCurenta.get("ruteRows");
  ruteRows.push(ruta.toJSON());
  cursaCurenta.set("ruteRows",ruteRows);
  showRuteRows(ruteRows);
  console.log(ruta.attributes);
}

function salveazaRuta(){
  cursaCurenta.set("auto_nr",$("#cursaAutomobile").val());
  cursaCurenta.set("startKm",$("#kmPlecare").val());
  cursaCurenta.set("finishKm",$("#kmSosire").val());
  cursaCurenta.set("startFuel",$("#combPlecare").val());
  cursaCurenta.set("finishFuel",$("#combSosire").val());
  cursaCurenta.set("number",$("#nrAutomobilCursa").val());
  cursaCurenta.set("date",$("#dataCursa").val());
  cursaCurenta.set("confirmare",$("#confirmareCursa").val());

  cursaCurenta.save();
}
function changeAuto(){
  _.each(autoturisme.toArray(), function(a){
    if(a.get("nr") == $("#cursaAutomobile").val()){
      cursaCurenta.set("auto_type",a.get("type"));
      cursaCurenta.set("auto_city_consumption",a.get("city_consumption"));
      cursaCurenta.set("auto_out_of_city_consumption",a.get("out_of_city_consumption"));
      cursaCurenta.set("auto_mixt_consumption",a.get("mixt_consumption"));
      cursaCurenta.set("auto_fuel_type",a.get("fuel_type"));
      return;
    }

  });
}
