var Furnizor = Backbone.Model.extend({
  url:"/furnizori",
  defaults:{
    name: ""
  },
  className: "row",
  validate: function(attrs){
    var errors = [];
    if(!attrs.name){
      errors.push({name:"addFurnizorName",message:"Furnizorul trebuie sa aiba un nume"});
    }
    return errors.length>0 ? errors : false;
  }
});

var Furnizori = Backbone.Collection.extend({
  model: Furnizor,
});

var furnizori = new Furnizori([{name:"OMV"}]);

var FurnizorView = Backbone.View.extend({
  tagName:"tr",
  model: new Furnizor({name:""}),
  template: _.template($("#tmplFurnizor").html()),
  events:{
    'click .saveFurnizor': 'saveFurnizor',
    'click .editFurnizor' : 'editFurnizor',
    'click .delFurnizor' : 'delFurnizor',
    'click .cancelFurnizor' : 'cancelFurnizor'
  },
  editFurnizor: function(e){
    $(".editFurnizor").hide();
    $(".delFurnizor").hide();
    this.$(".saveFurnizor").show();
    this.$(".cancelFurnizor").show();

    name=this.$(".name").html();

    this.$(".name").html("<input type='TEXT' class='form-control name-update' value='"+name+"'/>");
  },
  cancelFurnizor: function(e){
    $(".editFurnizor").show();
    $(".delFurnizor").show();
    $(".saveFurnizor").hide();
    $(".cancelFurnizor").hide();

    this.render();
  },
  delFurnizor : function(e){
    if(!confirm("Sigur doriti stergerea furnizorului selectat?")){
      this.cancelFurnizor();
      return;
    }
    this.model.destroy();
  },
  saveFurnizor: function(){
    this.model.set("name",this.$(".name-update").val());
    this.render();
    this.cancelFurnizor();
  },
  render: function(){
    var data = this.model ? this.model.toJSON() : {}
    this.$el.html(this.template(data));
    return this;
  }
});

var FurnizoriView = Backbone.View.extend({
  el: $("#container"),
  template: _.template($("#tmplFurnizori").html()),
  model: furnizori,
  initialize:function(){
    this.model.on("remove",this.render,this);
    self=this;
    this.model.on("invalid",function(error){
      var err = "";
      self.clearErrorSigns();
      _.each(error.validationError, function(error){
        err+=error.message+"\n";
        $("#div"+error.name).addClass("has-error");
      });
      alert(err);
    });
    this.render()
  },
  clearErrorSigns: function(){
    $("div .form-group").removeClass("has-error");
  },
  events:{
    "click #addFurnizor":'add'
  },
  add: function(e){
    e.preventDefault();
    var a = new Furnizor({
      name: $("#addFurnizorName").val(),
    });
    if(this.model.create(a).validationError==null){
      this.render();
    }
  },
  render: function(){
    $("#MainMenu").find('li').removeClass("active");
    $('a[href="#Furnizori"]').parent().addClass("active");

    $("#header").html("");

    var data = this.model? this.model.toJSON():{};
    this.$el.html(this.template(data));
    _.each(this.model.toArray(),function(f){
      if(f.validationError==null){
        $("#tblFurnizori tbody").append(new FurnizorView({model:f}).render().el);
      }
    });
    return this;
  }
});
