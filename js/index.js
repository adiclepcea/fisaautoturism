function isLoggedIn(){
  //for development only
  var at=true;
  //var at = Cookie.get("access-token");
  if (at){
    var menuView = new MenuView();
    menuView.render();
    return true;
  }
  var initialView = new NotLoggedView();
  initialView.render();
  return false;
}

var NotLoggedView = Backbone.View.extend({
  tagName: "span",
  el: $("#navbar"),
  template: _.template($("#tmplMenuNotLogged").html()),
  events: {
    'click #login':'login'
  },
  login: function(){
    alert("login")
  },
  render: function(){
    this.$el.html(this.template(this.model));
    return this;
  }
});

var MenuView = Backbone.View.extend({
  tagName: "span",
  el: $("#navbar"),
  template: _.template($("#tmplMenu").html()),
  render: function(){
    this.$el.html(this.template(this.model));
    return this;
  }
});

var HomeView = Backbone.View.extend({
    el: $("#container"),
    template: _.template($("#home").html()),
    render: function(){
      isLoggedIn();
      this.$el.html(this.template(this.model));
      return this;
    }
});

function formatDate(date,separator){
  console.log(date);
  var day = date.getDate();
  var month = date.getMonth()+1;
  var year = date.getFullYear();
  return (day<=9?"0"+day:day)+separator+(month<=9?"0"+month:month) +separator+year;
}

function formatDateDefault(date){
  console.log(date);
  var day = date.getDate();
  var month = date.getMonth()+1;
  var year = date.getFullYear();
  return year+"-"+(month<=9?"0"+month:month) +"-"+(day<=9?"0"+day:day);
}

$().ready(function(){

  var at = Cookie.get("access-token");
  if (at){
    var menuView = new MenuView();
    menuView.render();
  }else{
    var initialView = new NotLoggedView();
    initialView.render();
  }
  Backbone.history.start();
  flatpickr("#dataCursa");
});
