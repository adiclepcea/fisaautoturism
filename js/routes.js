var vFurnizori;
var vAutoturisme;
var vAngajati;
var vRute;
var vCurse;
var vCurseHeader;
var vCursaAlim;
var vCursaRute;

var AppRouter = Backbone.Router.extend({
  routes:{
    "Angajati":"showAngajati",
    "Autoturisme":"showAutoturisme",
    "Furnizori":"showFurnizori",
    "Rute":"showRute",
    "Curse":"showCurse",
    "CursaRute":"showRuteCursa",
    "CursaAlimentari":"showAlimetariCursa",
    "":"defaultRoute"
  },
  initialize: function(){
    vAngajati = new AngajatiView();
    vAutoturisme = new AutoturismeView();
    vFurnizori = new FurnizoriView();
    vRute = new RuteView();
    vCurse = new CurseView();
    vCurseHeader = new CurseHeaderView();
    vCursaAlim = new CursaAlimentariView();
  },
  defaultRoute: function(actions){
    var homeView = homeView || new HomeView();
    homeView.render();
  },
  showAngajati: function(){
    if(!isLoggedIn()){
      this.defaultRoute();
      return;
    }
    vAngajati.render();
  },
  showAutoturisme: function(){
    if(!isLoggedIn()){
      this.defaultRoute();
      return;
    }
    vAutoturisme.render();
  },
  showFurnizori: function(){
    if(!isLoggedIn()){
      this.defaultRoute();
      return;
    }
    vFurnizori.render();
  },
  showRute: function(){
    if(!isLoggedIn()){
      this.defaultRoute();
      return;
    }
    vRute.render();
  },
  showCurse: function(){
    if(!isLoggedIn()){
      this.defaultRoute();
      return;
    }
    vCurse.render();
    vCurseHeader.render();
    vCursaAlim.render();
  },
  showAlimetariCursa: function(){
    if(!isLoggedIn()){
      this.defaultRoute();
      return;
    }

    $("#cursaAlimentari").show();
    $("#cursaRute").hide();
    $("#tabCursaRute").removeClass("active");
    $("#tabCursaAlimentari").addClass("active");
  },
  showRuteCursa: function(){
    if(!isLoggedIn()){
      this.defaultRoute();
      return;
    }

    $("#cursaRute").show();
    $("#cursaAlimentari").hide();
    $("#tabCursaRute").addClass("active");
    $("#tabCursaAlimentari").removeClass("active");
  }
});

var router = new AppRouter();
