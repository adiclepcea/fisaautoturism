var Ruta = Backbone.Model.extend({
  url:"/rute",
  defaults:{
    origin: "",
    via: "",
    destination: "",
    kms: 0
  },
  className: "row",
  validate: function(attrs){
    var errors = [];
    if(!attrs.origin){
      errors.push({name:"addRuteOrigin",message:"Ruta trebuie sa aiba o origine"});
    }
    if(!attrs.destination){
      errors.push({name:"addRuteDestination",message:"Ruta trebuie sa aiba o destinatie"});
    }
    if(isNaN(attrs.kms) || attrs.kms==""){
      errors.push({name:"addRuteKms",message:"Ruta trebuie sa aiba o lungime in km"});
    }
    return errors.length>0 ? errors : false;
  }
});

var Rute = Backbone.Collection.extend({
  model: Ruta,
});

var rute = new Rute([{origin:"Arad",destination:"Curtici",kms:20}]);

var RutaView = Backbone.View.extend({
  tagName:"tr",
  model: new Ruta({origin:"",destination:"",kms:0}),
  template: _.template($("#tmplRuta").html()),
  events:{
    'click .saveRuta': 'saveRuta',
    'click .editRuta' : 'editRuta',
    'click .delRuta' : 'delRuta',
    'click .cancelRuta' : 'cancelRuta'
  },
  editRuta: function(e){
    $(".editRuta").hide();
    $(".delRuta").hide();
    this.$(".saveRuta").show();
    this.$(".cancelRuta").show();

    origin=this.$(".origin").html();
    via=this.$(".via").html();
    destination=this.$(".destination").html();
    kms=this.$(".kms").html();
    this.$(".origin").html("<input type='TEXT' class='form-control origin-update' value='"+origin+"'/>");
    this.$(".via").html("<input type='TEXT' class='form-control via-update' value='"+via+"'/>");
    this.$(".destination").html("<input type='TEXT' class='form-control destination-update' value='"+destination+"'/>");
    this.$(".kms").html("<input type='TEXT' class='form-control kms-update' value='"+kms+"'/>");
  },
  cancelRuta: function(e){
    $(".editRuta").show();
    $(".delRuta").show();
    $(".saveRuta").hide();
    $(".cancelRuta").hide();

    this.render();
  },
  delRuta : function(e){
    if(!confirm("Sigur doriti stergerea rutei selectate?")){
      this.cancelRuta();
      return;
    }
    this.model.destroy();
  },
  saveRuta: function(){
    this.model.set("origin",this.$(".origin-update").val());
    this.model.set("via",this.$(".via-update").val());
    this.model.set("destination",this.$(".destination-update").val());
    this.model.set("kms",this.$(".kms-update").val());
    this.render();
    this.cancelRuta();
  },
  render: function(){
    var data = this.model ? this.model.toJSON() : {}
    this.$el.html(this.template(data));
    return this;
  }
});

var RuteView = Backbone.View.extend({
  el: $("#container"),
  model: rute,
  destroy_view: function() {

    // COMPLETELY UNBIND THE VIEW
    this.undelegateEvents();

    this.$el.removeData().unbind();

    // Remove view from DOM
    this.remove();
    Backbone.View.prototype.remove.call(this);

  },
  template: _.template($("#tmplRute").html()),
  initialize:function(){
    this.model.on("remove",this.render,this);
    var self=this;
    this.model.on("invalid",function(error){
      var err = "";
      self.clearErrorSigns();
      _.each(error.validationError, function(error){
        err+=error.message+"\n";
        $("#div"+error.name).addClass("has-error");
      });
      alert(err);
    });
    this.render();
  },
  clearErrorSigns: function(){
    $("div .form-group").removeClass("has-error");
  },
  events:{
    "click #addRuta":'add'
  },
  add: function(e){
    e.preventDefault();
    var r = new Ruta({
      origin: $("#addRuteOrigin").val(),
      via: $("#addRuteVia").val(),
      destination: $("#addRuteDestination").val(),
      kms: $("#addRuteKms").val(),
    });

    if(this.model.create(r).validationError==null){
      this.render();
    }
  },
  render: function(){
    $("#MainMenu").find('li').removeClass("active");
    $('a[href="#Rute"]').parent().addClass("active");

    $("#header").html("");

    var data = this.model ? this.model.toJSON():{};
    this.$el.html(this.template(data));

    _.each(this.model.toArray(),function(r){
      if(r.validationError==null){
        $("#tblRute tbody").append(new RutaView({model:r}).render().el);
      }
    });

    return this;
  }
});
